<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('/vueGenerale.php',["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs"=> $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail() : void {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login); //appel au modèle pour gérer la BD
            if ($utilisateur == NULL) {
                ControleurUtilisateur::afficherVue('vueGenerale.php',["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
            }
            else {
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php","utilisateur" => $utilisateur]);
            }
        }
        else{
            ControleurUtilisateur::afficherVue('vueGenerale.php',["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php"]);
        }
    }

    public static function afficherFormulaireCreation() : void{
        self::afficherVue('vueGenerale.php',["titre" => "Création Utilisateur", "cheminCorpsVue" => "utilisateur/afficherFormulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire() : void{
        $nouvelleUtilisateur = new ModeleUtilisateur($_GET['login'],$_GET['nom'],$_GET['prenom']);
        $nouvelleUtilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        ControleurUtilisateur::afficherVue('vueGenerale.php',["titre" => "Utilisateur crée", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs"=> $utilisateurs]);
    }
}
?>
