<?php
/** @var ModeleUtilisateur $utilisateur */

    $prenomHTML = htmlspecialchars($utilisateur->getPrenom());
    $nomHTML = htmlspecialchars($utilisateur->getNom());
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    echo "<p> utilisateur $prenomHTML $nomHTML de login $loginHTML</p>";
?>
