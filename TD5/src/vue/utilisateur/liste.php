<?php
/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Modele\ModeleUtilisateur;

echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo <<< HTML
<li>
  <p> Utilisateur de login 
    <a href="controleurFrontal.php?action=afficherDetail&login=$loginURL">$loginHTML</a> 
  </p>
</li> 
HTML;
}
echo "</ol>";

echo '<a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur </a>';
?>



