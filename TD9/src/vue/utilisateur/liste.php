<?php
/** @var Utilisateur[] $utilisateurs */


use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $login = $utilisateur->getLogin();
    $loginHTML = htmlspecialchars($login);
    $loginURL = rawurlencode($login);
    echo "
<li xmlns=\"http://www.w3.org/1999/html\">
  <p> Utilisateur de login 
    <a href=\"controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=$loginURL\">$loginHTML</a> 
    </br>";

    if (ConnexionUtilisateur::estUtilisateur($login) | ConnexionUtilisateur::estAdministrateur()) {
        echo "<p>
    <a class=MiseAJour href=\"controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=$loginURL\">Mettre à jour </a> 
        <a class=\"supprimer\" href=\"controleurFrontal.php?controleur=utilisateur&action=utilisateurSupprime&login=$loginURL\">X</a>   
    </p>";
    }

echo "</li>";
}
echo "</ol>";

echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur </a>';
?>



