<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository{
    public function mettreAJour(AbstractDataObject $objet): void
    {
        $nomtable = $this->getNomTable();
        $parametres = array();
        foreach ($this->getNomsColonnes() as $colonne) {
            $parametres[] = $colonne . " = :" . $colonne . "Tag";
        }
        $parametre = join(", ", $parametres);
        $clePrimaire = $this->getNomClePrimaire();

        $sql = "UPDATE $nomtable SET $parametre WHERE $clePrimaire = :".$clePrimaire ."Tag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {
        $nomtable = $this->getNomTable();
        $nomsColonnes = join(",",$this->getNomsColonnes());
        $tagColonnes = ":" . join("Tag, :", $this->getNomsColonnes()) . "Tag";
        $sql = "INSERT INTO $nomtable ($nomsColonnes) VALUES ($tagColonnes)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);

        $pdoStatement->execute($values);

        return true;
    }

    public function supprimer(string $valeurClePrimaire): void
    {
        $nomTable = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();
        $sql = "DELETE FROM $nomTable WHERE $clePrimaire = :valeurClePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "valeurClePrimaire" => $valeurClePrimaire
        );
        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $identifiant): ?AbstractDataObject
    {
        $nomTable = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();
        $sql = "SELECT * from $nomTable WHERE $clePrimaire = :identifiant";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "identifiant" => $identifiant
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau) {
            return null;
        }
        return $this->construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $tableauUtilisateurs = array();
        $pdoStateement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM ". $this->getNomTable());
        // $pdoStatement->setFetchModœe(PDO::FETCH_ASSOC)
        foreach ($pdoStateement as $utilisateurFormatTableau) {
            $tableauUtilisateurs[] = $this->construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableauUtilisateurs;
    }

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;


}