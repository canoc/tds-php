<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_REQUEST['login'])) {
            $login = $_REQUEST['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            if ($utilisateur == NULL) {
                self::redirectionVersURL(ConfigurationSite::getURLAbsolue()."?messagesFlash[warning][]=Login inconnu");
            } else {
                self::afficherVueGenerale(["titre" => "Détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
            }
        } else {
            self::afficherErreur("Pas de login donné");
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVueGenerale(["titre" => "Création Utilisateur", "cheminCorpsVue" => "utilisateur/afficherFormulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        if ($_REQUEST["mdp"] != $_REQUEST["mdp2"]){
            self::afficherErreur("Mots de passe distincts");
            return;
        }
        if (!filter_var('bob@example.com', FILTER_VALIDATE_EMAIL)){
            self::afficherErreur("Email invalide");
            return;
        }
        $nouvelleUtilisateur = self::construireDepuisFormulaire($_REQUEST);
        if (!ConnexionUtilisateur::estAdministrateur()) {
            $nouvelleUtilisateur->setEstAdmin(false);
        }
        (new UtilisateurRepository())->ajouter($nouvelleUtilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Utilisateur crée", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }

    public static function utilisateurSupprime(): void
    {
        if (!isset($_REQUEST["login"])) {
            self::afficherErreur("Pas de login transmis");
            return;
        }
        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST["login"]) && !ConnexionUtilisateur::estAdministrateur()) {
            self::afficherErreur("Vous pouvez supprimer uniquement vos données");
            return;
        }
        $login = $_REQUEST['login'];
        (new UtilisateurRepository())->supprimer($login);
        self::redirectionVersURL(ConfigurationSite::getURLAbsolue()."?messagesFlash[success][]=L'utilisateur a bien été supprimé !");
    }

    public static function afficherFormulaireMiseAJour():void {
        $login = $_REQUEST['login'];
        if (!ConnexionUtilisateur::estUtilisateur($login) && !ConnexionUtilisateur::estAdministrateur()){
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            return;
        }
        if ((new UtilisateurRepository)->recupererParClePrimaire($_REQUEST["login"]) == null){
            self::afficherErreur("Login inconnu");
            return;
        }
        if (ConnexionUtilisateur::estUtilisateur($login)) {
            self::afficherVueGenerale(["titre" => "Formulaire de Mise à Jour", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", "login" => $login]);
        }
        else{
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
        }
    }

    public static function mettreAJour()
    {
        if (!(isset($_REQUEST['prenom']) && (isset($_REQUEST['nom'])) && (isset($_REQUEST['AncienMdp'])) && (isset($_REQUEST['mdp'])) && (isset($_REQUEST['mdp2'])) )) {
            self::afficherErreur("Données manquante dans le formulaire");
            return;
        }
        if ($_REQUEST["mdp"] != $_REQUEST["mdp2"]) {
            self::afficherErreur("Mot de passe de confirmation différent");
            return;
        }
        $utilisateur = (new UtilisateurRepository)->recupererParClePrimaire($_REQUEST["login"]);
        /** @var Utilisateur $utilisateur */
        if ( $utilisateur == null){
            self::afficherErreur("Login inconnu");
            return;
        }
        if (!ConnexionUtilisateur::estAdministrateur() && !MotDePasse::verifier($_REQUEST["AncienMdp"],$utilisateur->getMdpHache())) {
            self::afficherErreur("Mot de passe incorrect");
            return;
        }
        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST["login"]) && !ConnexionUtilisateur::estAdministrateur()) {
            self::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            return;
        }
        $utilisateur->mettreAJour($_REQUEST["nom"],$_REQUEST["prenom"],MotDePasse::hacher($_REQUEST["mdp"]));
        if (ConnexionUtilisateur::estAdministrateur()){
            $utilisateur->setEstAdmin(isset($_REQUEST["mdp"]));
        }
        if (isset($_REQUEST["email"]) && ($_REQUEST["email"] != $utilisateur->getEmail())){
            if (!filter_var('bob@example.com', FILTER_VALIDATE_EMAIL)){
                self::afficherErreur("Email invalide");
                return;
            }
            (new UtilisateurRepository())->modifierEmail($utilisateur,$_REQUEST["email"]);

        }
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Utilisateur mis à jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $utilisateur->getLogin(),"utilisateurs" => $utilisateurs]);
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $mdpHache = MotDePasse::hacher($_REQUEST["mdp"]);
        $utilisateur = new Utilisateur(
            $tableauDonneesFormulaire['login'],
            $tableauDonneesFormulaire['nom'],
            $tableauDonneesFormulaire['prenom'],
            $mdpHache,
            isset($tableauDonneesFormulaire["estAdmin"]),
            "", //email
            $tableauDonneesFormulaire['email'],
            MotDePasse::genererChaineAleatoire()
        );
        VerificationEmail::envoiEmailValidation($utilisateur);
        return $utilisateur;
    }

    public static function afficherFormulaireConnexion():void {
        self::afficherVueGenerale(["titre" => "Connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);

    }
    public static function connecter():void {
        if (!isset($_REQUEST["login"]) || !isset($_REQUEST["mdp"])) {
            self::afficherErreur("Login et/ou mot de passe manquant");
            return;
        }
        $utilisateur =  (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST["login"]);
        /** @var Utilisateur $utilisateur */
        if ($utilisateur == NULL || !MotDePasse::verifier($_REQUEST["mdp"],$utilisateur->getMdpHache())) {
            self::afficherErreur("Login et/ou mot de passe incorrect");
            return;
        }
        if (!VerificationEmail::aValideEmail($utilisateur)){
            self::afficherErreur("Email invalide");
            return;
        }
        ConnexionUtilisateur::connecter($_REQUEST["login"]);
        self::afficherVueGenerale(["titre" => "Connecté", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "utilisateur" => $utilisateur]);

        }

    public static function deconnecter(): void{
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "liste", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php", "utilisateurs" => $utilisateurs]);
    }

    public static function validerEmail(): void{
        if (!isset($_REQUEST["login"]) || !isset($_REQUEST["nonce"])){
            self::afficherErreur("Données incomplète");
            return;
        }
        $login = $_REQUEST["login"];
        if (!VerificationEmail::traiterEmailValidation($login,$_REQUEST["nonce"])){
            self::afficherErreur("erreur dans le traitement du mail");
            return;
        }
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        self::afficherVueGenerale(["titre" => "Détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
    }

}
