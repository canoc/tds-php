<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        Session::getInstance()->enregistrer(self::$cleConnexion,$loginUtilisateur);
    }
    public static function estConnecte(): bool
    {
        return Session::getInstance()->contient(self::$cleConnexion);
    }

    public static function deconnecter(): void
    {
        Session::getInstance()->supprimer(self::$cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        if (!self::estConnecte()) {
            return null;
        }
        return Session::getInstance()->lire(self::$cleConnexion);
    }

    public static function estUtilisateur($login): bool{
        return self::getLoginUtilisateurConnecte()==$login;
    }

    public static function estAdministrateur() : bool{
        $sql = "SELECT estAdmin FROM utilisateurs WHERE login = :login";
        $pdostatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array('login' => self::getLoginUtilisateurConnecte());
        $pdostatement->execute($values);
        $tableau = $pdostatement->fetch();
        if (!$tableau) return false;
        return $tableau["estAdmin"]==true;
    }



}

