<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class Utilisateur extends AbstractDataObject {

    private string  $login;
    private string  $nom;
    private string  $prenom;
    private bool $estAdmin;
    private String $email;
    private String $emailAValider;
    private String $nonce;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    private string $mdpHache;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom( string $nom) {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin( string $login): void
    {
        $this->login = substr($login,0,64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() :  string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom( string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null){
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    // un constructeur
    public function __construct(
        string  $login,
        string $nom,
        string $prenom,
        string $mdpHache,
        bool $estAdmin,
        String $email,
        String $emailAValider,
        String $nonce
   ) {
      $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = $mdpHache;
    }

    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }

    public function aValideEmail(): bool{
        return $this->email != "";
    }



    public function mettreAJour(string $nom, string $prenom, string $mdpHache){
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setMdpHache($mdpHache);
    }

/*
// Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() :  string  {
        return "utilisateur " . $this->prenom . " " . $this->nom . " de login " . $this->login;
    }*/


}