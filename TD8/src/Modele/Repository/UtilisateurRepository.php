<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository
{

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur(
            $utilisateurFormatTableau["login"],
            $utilisateurFormatTableau["nom"],
            $utilisateurFormatTableau["prenom"],
            $utilisateurFormatTableau["mdpHache"],
            $utilisateurFormatTableau["estAdmin"],
            $utilisateurFormatTableau["email"],
            $utilisateurFormatTableau["emailAValider"],
            $utilisateurFormatTableau["nonce"],
        );
    }

    /**
     * @return Trajet[]
     *//*
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array{
        $sql = "SELECT * FROM trajet t
                JOIN passager p ON t.id = p.trajetId
                WHERE p.passagerLogin = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "login" => $utilisateur->getLogin(),
        );

        $tableauTrajets = array();
        $pdoStatement->execute($values);
        foreach ($pdoStatement as $trajetFormatTableau){
            $tableauTrajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $tableauTrajets;
    }*/

    protected function getNomTable(): string
    {
        return 'utilisateurs';
    }

    protected function getNomClePrimaire(): string
    {
        return 'login';
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom","mdpHache","estAdmin", "email", "emailAValider", "nonce"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        $tags =  array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
        );

        if ($utilisateur->isEstAdmin()){
            $tags["estAdminTag"] = "1";
        }
        else{
            $tags["estAdminTag"] = "0";
        }
        $tags["emailTag"] = $utilisateur->getEmail();
        $tags["emailAValiderTag"] = $utilisateur->getEmailAValider();
        $tags["nonceTag"] = $utilisateur->getNonce();

        return $tags;
    }

    public function modifierEmail(Utilisateur $utilisateur, String $email): void {
        $sql = "UPDATE utilisateurs SET emailAValider = :email, nonce= :nonce WHERE login = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $nonce = MotDePasse::genererChaineAleatoire();
        $values = array(
            "email" => $email,
            "nonce" => $nonce,
            "login" => $utilisateur->getLogin()
        );
        $pdoStatement->execute($values);
        $utilisateur->setEmailAValider($email);
        $utilisateur->setNonce($nonce);
        VerificationEmail::envoiEmailValidation($utilisateur);
    }




}