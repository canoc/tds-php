<?php

namespace App\Covoiturage\Configuration;
class ConfigurationSite{

    static private int $dureeExpiration = 100;

    public static function getDureeExpiration(): int
    {
        return self::$dureeExpiration;
    }

    public static function getURLAbsolue(): String{
        return "http://localhost/tds-php/TD8/web/controleurFrontal.php";
    }

    public static function getDebug(): bool{
        return false;
    }





}