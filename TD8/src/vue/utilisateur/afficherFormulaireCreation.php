<form method="<?php if (ConfigurationSite::getDebug()) echo "get"; else echo "post";?>" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="hidden" name='action' value='creerDepuisFormulaire'/>
        <input type="hidden" name='controleur' value='utilisateur'/>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="leblancj" name="login" id="login_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prenom</label>
            <input class="InputAddOn-field" type="text" placeholder="Juste" name="prenom" id="prenom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom</label>
            <input class="InputAddOn-field" type="text" placeholder="Leblanc" name="nom" id="nom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <?php use App\Covoiturage\Configuration\ConfigurationSite;
        use App\Covoiturage\Lib\ConnexionUtilisateur;

        if(ConnexionUtilisateur::estAdministrateur()){
            echo <<<HTML
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id">
            </p>
            HTML;
        }?>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="" placeholder="toto@yopmail.com" name="email" id="email_id" required>
        </p>



        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>