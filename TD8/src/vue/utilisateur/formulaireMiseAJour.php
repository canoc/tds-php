<?php
 /** @var String $login */

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

$utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login)
/** @var Utilisateur $utilisateur */
?>

<form method="<?php if (ConfigurationSite::getDebug()) echo "get"; else echo "post";?>" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="hidden" name='action' value='mettreAJour'/>
        <input type="hidden" name='controleur' value='utilisateur'/>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($login)?>" name="login" id="login_id" readonly/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prenom</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom())?>" name="prenom" id="prenom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom())?>" name="nom" id="nom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="AncienMdp" id="mdp_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email">Email</label>
            <input class="InputAddOn-field" type="email" value="<?= htmlspecialchars($utilisateur->getEmail())?>" placeholder="toto@yopmail.com" name="email" id="email" required>
        </p>

        <?php
            if(ConnexionUtilisateur::estAdministrateur()){
                echo "
        <p class=\"InputAddOn\">
            <label class=\"InputAddOn-item\" for=\"estAdmin_id\">Administrateur</label>
            <input class=\"InputAddOn-field\" type=\"checkbox\" name=\"estAdmin\" id=\"estAdmin_id\"";
            if($utilisateur->isEstAdmin()){echo "checked";}
            echo ">        
        </p>";
            }
            ?>



        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>