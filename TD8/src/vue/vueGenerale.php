<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <?php
        /**
        * @var string $titre
        */

        use App\Covoiturage\Lib\ConnexionUtilisateur;

        ?>
        <title><?php echo $titre; ?></title>
        <link rel="stylesheet" href="../ressources/styleGenerale.css">
    </head>

    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/heart.png"></a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/add-user.png"></a>
                    </li>
                    <?php
                    if (!ConnexionUtilisateur::estConnecte()){
                        echo "                    
                    <li>
                        <a href=\"controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur\"><img src=\"../ressources/img/enter.png\"></a>
                    </li>";
                    }
                    else{
                        $loginURL = rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte());
                        echo "                    
                    <li>
                        <a href=\"controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=$loginURL\"><img src=\"../ressources/img/user.png\"></a>
                    </li>
                    <li>
                        <a href=\"controleurFrontal.php?action=deconnecter&controleur=utilisateur\"><img src=\"../ressources/img/logout.png\"></a>
                    </li>";
                    }
                    ?>
                </ul>

            </nav>


        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de Corentin
            </p>
        </footer>
    </body>

</html>

