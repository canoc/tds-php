<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class UtilisateurRepository extends AbstractRepository
{
    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur
    {
        return new Utilisateur($utilisateurFormatTableau["login"], $utilisateurFormatTableau["nom"], $utilisateurFormatTableau["prenom"]);
    }

    /**
     * @return Trajet[]
     *//*
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array{
        $sql = "SELECT * FROM trajet t
                JOIN passager p ON t.id = p.trajetId
                WHERE p.passagerLogin = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "login" => $utilisateur->getLogin(),
        );

        $tableauTrajets = array();
        $pdoStatement->execute($values);
        foreach ($pdoStatement as $trajetFormatTableau){
            $tableauTrajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $tableauTrajets;
    }*/

    protected function getNomTable(): string
    {
        return 'utilisateurs';
    }

    protected function getNomClePrimaire(): string
    {
        return 'login';
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }




}