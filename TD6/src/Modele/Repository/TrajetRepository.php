<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class TrajetRepository extends AbstractRepository{
    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );
        $passagers = TrajetRepository::recupererPassagers($trajet);
        $trajet->setPassagers($passagers);
        return $trajet;
    }

    /**
     * @return Trajet[]
     */
    /*
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = TrajetRepository::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    /**
     * @return Utilisateur[]
     */
    private static function recupererPassagers(Trajet $trajet) : array {
        $sql = "SELECT passagerLogin 
                FROM passager p 
                JOIN trajet t ON t.id = p.trajetId 
                WHERE t.id = :trajetId";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "trajetId" => $trajet->getId(),
        );

        $passagers = array();
        $pdoStatement->execute($values);

        $PassagerFormatTableau = $pdoStatement->fetch();
        while ($PassagerFormatTableau){
            $passagers[] = (new UtilisateurRepository())->recupererParClePrimaire($PassagerFormatTableau["passagerLogin"]);
            $PassagerFormatTableau = $pdoStatement->fetch();
        }

        return $passagers;
    }

    protected function getNomTable(): string
    {
        return 'trajet';
    }

    protected function getNomClePrimaire(): string
    {
        return 'id';
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        $tags = array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin()
        );
        if ($trajet->isNonFumeur()){
            $tags["nonFumeurTag"] = "1";
        }
        else{
            $tags["nonFumeurTag"] = "0";
        }
        return $tags;
    }
}