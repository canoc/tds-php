<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    private static function afficherVueGenerale(array $parametres = []): void
    {
        self::afficherVue('vueGenerale.php', $parametres);
    }

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Liste des trajet", "cheminCorpsVue" => "trajet/liste.php", "trajets" => $trajets]);  //"redirige" vers la vue
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        self::afficherVueGenerale(["titre" => "Erreur", "cheminCorpsVue" => "trajet/erreur.php", 'messageErreur' => $messageErreur]);
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id); //appel au modèle pour gérer la BD
            if ($id == NULL) {
                self::afficherErreur("Pas de trajet trouvé");
            } else {
                self::afficherVueGenerale(["titre" => "Détail d'un trajet", "cheminCorpsVue" => "trajet/detail.php", "trajet" => $trajet]);
            }
        } else {
            self::afficherErreur("Pas d'identifiant donné");
        }
    }

    public static function trajetSupprime(): void
    {
        $id = $_GET['id'];
        (new TrajetRepository())->supprimer($id);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Trajet supprimée","cheminCorpsVue" => "trajet/trajetSupprime.php", "trajets" => $trajets,"id" => $id]);
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVueGenerale(["titre" => "Création Trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $nouveauTrajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($nouveauTrajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Trajet crée", "cheminCorpsVue" => "trajet/trajetCree.php", "trajets" => $trajets]);
    }

    public static function afficherFormulaireMiseAJour():void {
        $id = $_GET['id'];
        self::afficherVueGenerale(["titre" => "Formulaire de Mise à Jour", "cheminCorpsVue"=> "trajet/formulaireMiseAJour.php", "id" => $id]);
    }

    /**
     * @return Trajet
     * @throws \Exception
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        return new Trajet(
            $id,
            $tableauDonneesFormulaire["depart"],
            $tableauDonneesFormulaire["arrivee"],
            new DateTime($tableauDonneesFormulaire["date"]),
            $tableauDonneesFormulaire["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire["conducteurLogin"]),
            isset($tableauDonneesFormulaire["nonFumeur"]),
        );
    }

    public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (New TrajetRepository())->mettreAJour($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Trajet mis à jour", "cheminCorpsVue" => "trajet/trajetMisAJour.php", "id" => $trajet->getId(),"trajets" => $trajets]);
    }
}
?>
