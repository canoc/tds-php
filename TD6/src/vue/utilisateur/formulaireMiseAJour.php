<?php
 /** @var String $login */

use App\Covoiturage\Modele\Repository\UtilisateurRepository;

$utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login)
?>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="hidden" name='action' value='mettreAJour'/>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($login)?>" name="login" id="login_id" readonly/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prenom</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom())?>" name="prenom" id="prenom_id" required/>
        </p>

        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom())?>" name="nom" id="nom_id" required/>
        </p>

        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>