<?php
/** @var Trajet[] $trajets */


use App\Covoiturage\Modele\DataObject\trajet;

echo "<ol>";
foreach ($trajets as $trajet) {
    $idTrajetHTML = htmlspecialchars($trajet->getId());
    $idTrajetURL = rawurlencode($trajet->getId());
    $departHTML = htmlspecialchars($trajet->getDepart());
    $arriveHTML = htmlspecialchars($trajet->getArrivee());
    echo <<< HTML
<li xmlns="http://www.w3.org/1999/html">
    <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=$idTrajetURL">Trajet n°$idTrajetHTML ($departHTML -> $arriveHTML) </a>
    </br>
    <p>
    
          <a class=MiseAJour href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=$idTrajetURL">Mettre à jour </a>
          <a class="supprimer" href="controleurFrontal.php?controleur=trajet&action=trajetSupprime&id=$idTrajetHTML"> X</a>
    </p>
</li> 
HTML;
}
echo "</ol>";

echo '<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Créer un trajet </a>';


?>
