<?php

/** @var Trajet $trajet */

use App\Covoiturage\Modele\DataObject\Trajet;

$statutFumeur = "";
if ($trajet->isNonFumeur()){
    $statutFumeur = " non";
}

$prenomConducteurHTML = htmlspecialchars($trajet->getConducteur()->getPrenom());
$nomConducteurHTML = htmlspecialchars($trajet->getConducteur()->getNom());

echo "<p> Le trajet$statutFumeur fumeur du {$trajet->getDate()->format("d/m/Y")} partira de {$trajet->getDepart()} pour aller à {$trajet->getArrivee()} (conducteur: $prenomConducteurHTML $nomConducteurHTML).";
