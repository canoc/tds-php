<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass();
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Controleur\ControleurUtilisateur;

// On récupère le controleur passée dans l'URL
if (isset($_GET['controleur'])){
    $controleur = $_GET['controleur'];
}
else{
    $controleur = 'utilisateur';
}

// On récupère l'action passée dans l'URL
if (isset($_GET['action'])){
    $action = $_GET['action'];
}
else{
    $action = 'afficherListe';
}

$nomClasseControleur = 'App\Covoiturage\Controleur\\Controleur' . ucfirst($controleur);
if (class_exists($nomClasseControleur)) {
    // Appel de la méthode statique $action du controleur
    if (in_array($action, get_class_methods($nomClasseControleur))) {
        $nomClasseControleur::$action();
    } else {
        $nomClasseControleur::afficherErreur("Action invalide");
    }
}
else{
    ControleurUtilisateur::afficherErreur("Controleur invalide");
}

?>
