<?php
require_once "ConnexionBaseDeDonnees.php";
//require_once "Trajet.php";
class ModeleUtilisateur {

    private string  $login;
    private string  $nom;
    private string  $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom( string $nom) {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin( string $login): void
    {
        $this->login = substr($login,0,64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() :  string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom( string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null){
            $this->trajetsCommePassager = $this::recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    // un constructeur
    public function __construct(
        string  $login,
        string $nom,
        string $prenom,
   ) {
      $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }
/*
// Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() :  string  {
        return "utilisateur " . $this->prenom . " " . $this->nom . " de login " . $this->login;
    }*/

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        $utilisateur = new ModeleUtilisateur($utilisateurFormatTableau["login"],$utilisateurFormatTableau["nom"],$utilisateurFormatTableau["prenom"]);
        return $utilisateur;
    }

    public static function recupererUtilisateurs(){
        $tableauUtilisateurs = array();
        $pdoStateement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateurs");
        // $pdoStatement->setFetchModœe(PDO::FETCH_ASSOC)
        foreach ($pdoStateement as $utilisateurFormatTableau){
            $tableauUtilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableauUtilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateurs WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if (!$utilisateurFormatTableau){
            return null;
        }
        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): void{
        $sql = "INSERT INTO utilisateurs (login, nom, prenom) VALUES (:login, :nom, :prenom)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom
        );

        $pdoStatement->execute($values);
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array{
        $sql = "SELECT * FROM trajet t
                JOIN passager p ON t.id = p.trajetId
                WHERE p.passagerLogin = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "login" => $this->login,
        );

        $tableauTrajets = array();
        $pdoStatement->execute($values);
        foreach ($pdoStatement as $trajetFormatTableau){
            $tableauTrajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $tableauTrajets;
    }


}