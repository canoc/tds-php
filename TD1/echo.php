<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;
          $prenom = "Marc";

        echo "Bonjour\n " . $prenom;
        echo "Bonjour\n $prenom";
        echo 'Bonjour\n $prenom';

        echo $prenom;
        echo "$prenom";

        $nom = "Mouse";
        $prenom = "Mickey";
        $login = "hoho!";

        $utilisateur1 = [
                "nom" => "Duck",
            "prenom" => "Donald",
            "login" => "quack!"
                ];
        $utilisateur2 = [
            "nom" => "Duck",
            "prenom" => "Daysi",
            "login" => "quack!"
        ];


        $utilisateurs = [
                0 => $utilisateur1,
            1 => $utilisateur2
        ];

        ?>
    <p> <?php echo "Utilisateur $prenom $nom de login $login"?> </p>
    <p> <?php echo "Utilisateur {$utilisateur1["prenom"]} {$utilisateur1["nom"]} de login {$utilisateur1["login"]}'"?> </p>

    <h2>Liste des utilisateurs</h2>

    <ul> <?php
        if (empty($utilisateurs)){
            echo "Il n'y a aucun utilisateur";
        }
        else{
            foreach ($utilisateurs as $utilisateur) {
                echo "<li>";
                foreach ($utilisateur as $key => $value) {
                    echo $key . " => " . $value . " ";
                }
                echo "</li>";
            }
        }


        ?></ul>
    </body>
</html> 
