<?php
namespace App\Covoiturage\Modele\DataObject;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class Utilisateur extends AbstractDataObject {

    private string  $login;
    private string  $nom;
    private string  $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom( string $nom) {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin( string $login): void
    {
        $this->login = substr($login,0,64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() :  string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom( string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager == null){
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


    // un constructeur
    public function __construct(
        string  $login,
        string $nom,
        string $prenom,
   ) {
      $this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }
/*
// Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() :  string  {
        return "utilisateur " . $this->prenom . " " . $this->nom . " de login " . $this->login;
    }*/


}