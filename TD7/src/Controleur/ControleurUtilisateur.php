<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php", "utilisateurs" => $utilisateurs]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['login'])) {
            $login = $_GET['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            if ($utilisateur == NULL) {
                self::afficherErreur("Pas d'utilisateur trouvé");
            } else {
                self::afficherVueGenerale(["titre" => "Détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php", "utilisateur" => $utilisateur]);
            }
        } else {
            self::afficherErreur("Pas de login donné");
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVueGenerale(["titre" => "Création Utilisateur", "cheminCorpsVue" => "utilisateur/afficherFormulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void
    {
        $nouvelleUtilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($nouvelleUtilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Utilisateur crée", "cheminCorpsVue" => "utilisateur/utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }

    public static function utilisateurSupprime(): void
    {
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Utilisateur supprimée","cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", "utilisateurs" => $utilisateurs,"login" => $login]);
    }

    public static function afficherFormulaireMiseAJour():void {
        $login = $_GET['login'];
        self::afficherVueGenerale(["titre" => "Formulaire de Mise à Jour", "cheminCorpsVue"=> "utilisateur/formulaireMiseAJour.php", "login" => $login]);
    }

    public static function mettreAJour()
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (New UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVueGenerale(["titre" => "Utilisateur mis à jour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $utilisateur->getLogin(),"utilisateurs" => $utilisateurs]);
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
        return $utilisateur;
    }
/**
    public static function deposerCookie(){
        Cookie::enregistrer("cookie", new Utilisateur("michelP","polnareff","michel"));
    }

    public static function lireCookie(){
         echo Cookie::lire("cookie")->getNom();
    }**/
}
