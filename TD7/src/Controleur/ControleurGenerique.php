<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique{

    public static function afficherErreur(string $messageErreur = ""): void
    {
        ControleurGenerique::afficherVueGenerale(["titre" => "Erreur", "cheminCorpsVue" => "utilisateur/erreur.php", 'messageErreur' => $messageErreur]);
    }

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    protected static function afficherVueGenerale(array $parametres = []): void
    {
        self::afficherVue('vueGenerale.php', $parametres);
    }

    public static function afficherFormulairePreference(): void{
        self::afficherVueGenerale(["titre" => "formulaire de preference", "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference(): void{
        $controleur = $_GET['controleur_defaut'];
        PreferenceControleur::enregistrer($controleur);
        self::afficherVueGenerale(["titre" => "preférence enregistrée", "cheminCorpsVue" => "preferenceEnregistree.php"]);
    }
}