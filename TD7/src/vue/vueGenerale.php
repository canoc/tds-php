<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <?php
        /**
        * @var string $titre
        */
        ?>
        <title><?php echo $titre; ?></title>
        <link rel="stylesheet" href="../ressources/styleGenerale.css">
    </head>

    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li><li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/heart.png"></a>
                    </li>
                </ul>

            </nav>


        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de Corentin
            </p>
        </footer>
    </body>

</html>

