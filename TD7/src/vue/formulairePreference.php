<?php use App\Covoiturage\Lib\PreferenceControleur;?>

<form method="get">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type="hidden" name='action' value='enregistrerPreference'/>

        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php

        if(PreferenceControleur::existe() && PreferenceControleur::lire()=='utilisateur'){echo "checked";} ?>>
        <label for="utilisateurId">Utilisateur</label>

        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php

        if(PreferenceControleur::existe() && PreferenceControleur::lire()=='trajet'){echo "checked";} ?>>
        <label for="trajetId">Trajet</label>

        <p>
            <input type="submit" value="Envoyer" />
        </p>

    </fieldset>
</form>