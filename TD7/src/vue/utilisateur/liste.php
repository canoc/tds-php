<?php
/** @var Utilisateur[] $utilisateurs */


use App\Covoiturage\Modele\DataObject\Utilisateur;

echo "<ol>";
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo <<< HTML
<li xmlns="http://www.w3.org/1999/html">
  <p> Utilisateur de login 
    <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=$loginURL">$loginHTML</a> 
    </br>
    <p>
    <a class=MiseAJour href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=$loginURL">Mettre à jour </a> 
        <a class="supprimer" href="controleurFrontal.php?controleur=utilisateur&action=utilisateurSupprime&login=$loginURL">X</a>   
    </p>
</li> 
HTML;
}
echo "</ol>";

echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur </a>';
?>



